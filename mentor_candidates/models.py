from django.db import models
from django.core import validators


class Mentor(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.CharField(max_length=200, validators=[validators.validate_email])
    description = models.TextField(blank=True)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Opinion(models.Model):
    mentor = models.ForeignKey(Mentor)
    name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.name
